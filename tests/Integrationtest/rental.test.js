const {
    Rental
} = require('../../model/rental')
const {
    User
} = require('../../model/user')
const {
    Movie
} = require('../../model/movie')
const mongoose = require('mongoose')
const request = require('supertest')
const moment = require('moment')


describe('/api/rentals', () => {
    let server
    let movieId
    let customerId
    let rental
    let token
    const exec = () => {
        return request(server)
            .post('/api/returns')
            .set('x-auth-token', token)
            .send({
                customerId,
                movieId
            });
    };

    beforeEach(async () => {
        server = require('../../index')
        customerId = mongoose.Types.ObjectId()
        movieId = mongoose.Types.ObjectId()

        movie = new Movie({
            _id: movieId,
            title: '12345',
            dailyRentalRate: 2,
            genre: {
                name: '12345'
            },
            numberInStock: 11
        });

        await movie.save();



        token = new User().generateAuthToken();
        rental = new Rental({
            customer: {
                _id: customerId,
                name: '12345',
                phone: '12345'


            },
            movie: {
                _id: movieId,
                title: '12345',
                dailyRentalRate: 2,
                genre: {
                    name: '12345',
                    numberInStock: 10

                }


            }

        })
        await rental.save()
    })
    afterEach(async () => {
        await server.close();
        await Rental.remove({});
    })
    it('shuold return 401 if client is not loged in', async () => {
        token = ''

        const res = await exec()

        expect(res.status).toBe(401)


    })

    it('shuold return 400 if customer id is not provide ', async () => {
        customerId = ''
        const res = await exec()
        expect(res.status).toBe(400)


    })
    it('shuold return 400 if movieid id is not provide ', async () => {
        movieId = ''

        const res = await exec()

        expect(res.status).toBe(400)


    })
    it('shuold return 404 if rental movie not found', async () => {

        await Rental.remove({});

        const res = await exec();

        expect(res.status).toBe(404);


    })


    it('shuold return 400 if return is already proccesed', async () => {
        rental.dateReturned = new Date()

        await rental.save()


        const res = await exec();

        expect(res.status).toBe(400);


    })


    it('shuold show the return date if valid input is provided', async () => {

        const res = await exec()
        const rentalmovie = await Rental.findById(rental._id)
        const diff = new Date() - rentalmovie.dateReturned

        expect(diff).toBeLessThan(10 * 1000)


    })
    it('shuold set rental fee if input is valid', async () => {
        rental.dateOut = moment().add(-7, 'days').toDate()
        await rental.save()
        const res = await exec()
        const rentalmovie = await Rental.findById(rental._id)
        const diff = new Date() - rentalmovie.dateReturned

        expect(rentalmovie.rentalFee).toBe(14);


    })

    it('shuold increase the movie stocks valid', async () => {


        const res = await exec()
        const movieindb = await Movie.findById(movieId)


        expect(movieindb.numberInStock).toBe(movie.numberInStock + 1)


    })

    it('shuold return the rental if input is valid', async () => {


        const res = await exec()
        const rentalindb = await Rental.findById(rental._id)

        expect(Object.keys(res.body)).toEqual(expect.arrayContaining(['dateOut', 'rentalFee', 'customer', 'movie', 'dateReturned']))


    })






})