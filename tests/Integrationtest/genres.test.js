let server;
const request = require('supertest')
const {
    Genre
} = require('../../model/genres')
const {
    User
} = require('../../model/user')
const mongoose = require('mongoose')
// const User=require('../../')

let token;
const execute=async()=>{
    return await request(server)
    .post('/api/genres').set('x-auth-token', token).send({
        name: 'genre1'
    })
}
beforeEach(()=>{
    token = new User().generateAuthToken()
})

describe('api/genres', () => {
    beforeEach(() => {
        server = require('../../index')
    })
    afterEach(async () => {
        await server.close()
        await Genre.remove()
    })
    describe('GET /', () => {
        it('should return all genres', async () => {
            await Genre.collection.insertMany([{
                    name: 'genre1'
                },
                {  
                    name: 'genre2'
                }
            ])
            const res = await request(server).get('/api/genres')
            expect(res.status).toBe(200)
            //  console.log(res.body)
            //  expect(res.body.length).toBe(2)
            expect(res.body.some(e => {
                return e.name === 'genre2'
            })).toBeTruthy()
        })
    })
    describe('GET /:id', () => {
        it('return genre if valid id is provided', async () => {
            const genre = new Genre({
                name: 'genre1'
            });
            await genre.save();

            const res = await request(server).get('/api/genres/' + genre._id);

            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('name', genre.name);
        })
        it('return genre if valid id is provided', async () => {
            const genre = new Genre({
                name: 'genre1'
            });
            await genre.save();

            const res = await request(server).get('/api/genres/1');

            expect(res.status).toBe(404);
            // expect(res.body).toHaveProperty('name', genre.name);     
        })

    });

    describe('POST /', () => {
        it('should return 404 if user is not loged in ', async () => {
            const res = await request(server)
                .post('/api/genres').send({
                    name: 'genres1'
                })
            expect(res.status).toBe(401)
        })

        it('should return 400 if genre is more then 50', async () => {
            const token = new User().generateAuthToken()
            const name = new Array(5).join('a')
            const res = await request(server)
                .post('/api/genres').set('x-auth-token', token).send({
                    name: name
                })
            expect(res.status).toBe(400)
        })


        it('should save the genre if it;s valid', async () => {
            const token = new User().generateAuthToken()
         
            const res = await request(server)
                .post('/api/genres').set('x-auth-token', token).send({
                    name: 'genre1'
                })

             const genre=   Genre.find({name:'genre1'})
            expect(genre).not.toBeNull()
        })

        it('should save the genre if it;s valid', async () => {
            const token = new User().generateAuthToken()
         
        const res= await execute()

          
            expect(res.body).toHaveProperty('_id')

            expect(res.body).toHaveProperty('name')
        })

    })



})