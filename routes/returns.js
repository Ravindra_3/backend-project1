const express = require('express')

const router = express.Router()
const {
    Rental
} = require('../model/rental')
const {
    Movie
} = require('../model/movie')
const moment = require('moment')

const auth = require('../middilware/auth')
router.post('/', auth, async (req, res) => {
    if (!req.body.customerId) {
        return res.status(400).send('customerid is not porbided')
    }
    if (!req.body.movieId) {
        return res.status(400).send('movieid not porbided')
    }

    const rental = await Rental.findOne({
        "customer._id": req.body.customerId,
        "movie._id": req.body.movieId
    })

    if (!rental) {
        return res.status(404).send('Rental not found')
    }

    if (rental.dateReturned) {
        return res.status(400).send('Return alreday proccesd')
    }

    rental.dateReturned = new Date()
    const rentaldays = moment().diff(rental.dateOut, 'days')
    rental.rentalFee = rental.movie.dailyRentalRate * rentaldays
    await rental.save()

   await Movie.update({_id:rental.movie._id},{
        $inc:{
            numberInStock:1
        }
    })


    await res.status(200).send(rental)
})


// function validateReturn(req){
//     const schema={
//         customerId:Joi.objectId().required(),
//         customerId:Joi.objectId().required()
//     }
//     return Joi.validate(genre,schema)
// }

module.exports = router