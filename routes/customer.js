const {
    Customer,
    validate
} = require('../model/customer')
// const mongoose=require('mongoose')
const express = require('express')

const router = express.Router()
router.get('/', async (req, res) => {

    const customer = await Customer.find().sort('name')
    res.send(customer)
})

router.post('/', async (req, res) => {

    // console.log(req.body.name)
    const {
        error
    } = validate(req.body)

    if (error) {
        return res.status(400).send(error.details[0])
    }

    let customer = new Customer({
        name: req.body.name,
        isGold: req.body.isGold,
        phone: req.body.phone
    })

    customer = await customer.save()

    res.send(customer)
})

router.put('/:id', async (req, res) => {
    const {
        error
    } = validate(req.body)

    if (error) {
        return res.status(400).send(error.details[0])
    }

    const customer = await Customer.findByIdAndUpdate(req.params.id, {
        name: req.body.name,
        isGold: req.body.isGold,
        phone: req.body.phone
    }, {
        new: true
    })
    if (!customer) return res.status(404).send('The customer with the given ID was not found.');

    res.send(customer);
})

router.delete('/:id', async (req, res) => {
    const customer = await Customer.findByIdAndRemove(req.param.id)
    if (!customer) {
        return res.status(404).send('the customer with id not found')
    }
    res.send(customer)
})

router.get('/:id', async (req, res) => {
    const customer = await Customer.findById(req.param.id)
    if (!customer) {
        return res.status(404).send('the customer with id not found')
    }
    res.send(customer)
})

module.exports = router