const {
    Genre,
    validate
} = require('../model/genres')
const auth=require('../middilware/auth')
const express = require('express')
const validateOBjectid=require('../middilware/validateobject')
const router = express.Router()
const asyncMiddleware = require('../middilware/async')
const mongoose=require('mongoose')

router.get('/', async (req, res) => {
    const genres = await Genre.find().sort('name')
    res.send(genres)
  })
  


router.post('/',auth, async (req, res) => {

    const {
        error
    } = validate(req.body)
    if (error) {
        return res.status(400).send(error.details[0])
    }
    let genre = new Genre({
        name: req.body.name
    })
    genre = await genre.save()
   
    res.send(genre)

})

router.put('/:id', async (req, res) => {
    const {
        error
    } = validate(req.body)


    if (error) {
        return res.status(400).send(error.details[0].message)
    }

    const genre = await Genre.findByIdAndUpdate(req.params.id, {
        name: req.body.name
    }, {
        new: true
    });

    if (!genre) {

        return res.status(404).send('The genre with given id is not found')
    }

    res.send(genre)
});

router.delete('/:id', async (req, res) => {
    const genre = await Genre.findByIdAndRemove(req.params.id)

    if (!genre) {
        return res.status(404).send('The genre with given id is not found')
    }

    res.send(genre)
})

router.get('/:id',validateOBjectid, async (req, res) => {


    const genre = await Genre.findById(req.params.id);

    if (!genre) {

        return res.status(404).send('The genre with given id is not found')
    }

    res.send(genre)
})

module.exports = router