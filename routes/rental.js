
const {Rental, validate} = require('../model/rental')

const {Movie} = require('../model/movie')
const {Customer} = require('../model/customer')

const express = require('express')
const router = express.Router()

router.get('/', async (req, res) => {
  const rentals = await Rental.find().sort('-dateOut')
  res.send(rentals)
})

router.post('/', async (req, res) => {
  const { error } = validate(req.body)
  if (error) {
      return res.status(400).send(error.details[0].message)}

  const customer = await Customer.findById(req.body.customerId)
  if (!customer){ 
      return res.status(400).send('invalid customer')}

  const movie = await Movie.findById(req.body.movieId)
  if (!movie) {
      return res.status(400).send('invalid movie')}

  if (movie.numberInStock == 0) {
      
    return res.status(400).send('movies not instock')}

  let rental = new Rental({ 
    customer: {
      _id: customer._id,
      name: customer.name, 
      phone: customer.phone
    },
    movie: {
      _id: movie._id,
      title: movie.title,
      dailyRentalRate: movie.dailyRentalRate
    }
  })
  rental = await rental.save()

  movie.numberInStock --
  movie.save()
  
  res.send(rental)
})

router.get('/:id', async (req, res) => {
    // console.log(req.params.id)
  const rental = await Rental.findById(req.params.id)

  if (!rental){
       return res.status(404).send('The rental with the given id is notfound.')}

  res.send(rental)
})

module.exports = router