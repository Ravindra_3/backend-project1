const express = require('express')
const app = express()

require('./starttup/routes')(app)
require('./starttup/db')()
require('./starttup/config')()
require('./starttup/logging')()
require('./starttup/prod')(app)


const port =8000
const server=app.listen(port, () => console.log(`Listening on port ${port}.`))

module.exports=server

